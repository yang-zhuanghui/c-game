#if 1
/*【例11-10】建立一个学生成绩信息（包括学号、姓名、成绩）的单向链表，学生数据按学号由小到大顺序排列，要求实现对成绩信息的插入、修改、删除和遍历操作。*/

/* 用链表实现学生成绩信息的管理  */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct stud_node{
  int num;
  char name[20];
  int score;
  struct stud_node *next;
};
struct stud_node * Create_Stu_Doc();  /* 新建链表 */
struct stud_node * InsertDoc(struct stud_node * head, struct stud_node *stud); /* 插入 */
struct stud_node * DeleteDoc(struct stud_node * head, int num);  /* 删除 */
void Print_Stu_Doc(struct stud_node * head);  /* 遍历 */

int main(void)
{
  struct stud_node *head, *p;
  int choice, num, score;
  char name[20];
  int size = sizeof(struct stud_node);

  do{
       printf("1:Create 2:Insert 3:Delete 4:Print 0:Exit\n");
       scanf("%d", &choice);
       switch(choice){
	     case 1:
            head = Create_Stu_Doc();
            break;
	     case 2:
            printf("Input num,name and score:\n");
            scanf("%d%s%d", &num,name, &score);
            p = (struct stud_node *) malloc(size);
            p->num = num;
            strcpy(p->name, name);
            p->score = score;
            head = InsertDoc(head, p);
            break;
	     case 3:
            printf("Input num:\n");
            scanf("%d", &num);
            head = DeleteDoc(head, num);
            break;
         case 4:
            Print_Stu_Doc(head);
            break;
         case 0:
            break;
	   }
  }while(choice != 0);

  return 0;
}

/*新建链表*/
struct stud_node * Create_Stu_Doc()
{
    struct stud_node * head,*p;
    int num,score;
    char  name[20];
    int size = sizeof(struct stud_node);

    head = NULL;
    printf("Input num,name and score:\n");
    scanf("%d%s%d", &num,name, &score);
    while(num != 0){
       p = (struct stud_node *) malloc(size);
       p->num = num;
       strcpy(p->name, name);
       p->score = score;
       head = InsertDoc(head, p);    /* 调用插入函数 */
       scanf("%d%s%d", &num, name, &score);
   }
   return head;
}

/* 插入操作 */
struct stud_node * InsertDoc(struct stud_node * head, struct stud_node *stud)
{
   struct stud_node *ptr ,*ptr1, *ptr2;

    ptr2 = head;
    ptr = stud; 				/* ptr指向待插入的新的学生记录结点 */
    /* 原链表为空时的插入 */
    if(head == NULL){
        head = ptr; 			/* 新插入结点成为头结点 */
        head->next = NULL;
    }
    else{ 					    /* 原链表不为空时的插入 */
         while((ptr->num > ptr2->num) && (ptr2->next != NULL)){
            ptr1 = ptr2; 		/* ptr1, ptr2各后移一个结点 */
            ptr2 = ptr2->next;
         }
         if(ptr->num <= ptr2->num){ 	/* 在ptr1与ptr2之间插入新结点 */
            if(head == ptr2)  head = ptr;
            else ptr1->next = ptr;
            ptr->next = ptr2;
        }
        else{						    /* 新插入结点成为尾结点 */
            ptr2->next = ptr;
            ptr->next = NULL;
        }
    }
    return head;
}

/* 删除操作 */
struct stud_node * DeleteDoc(struct stud_node * head, int num)
{
    struct stud_node *ptr1, *ptr2;

    /* 要被删除结点为表头结点 */
    while(head != NULL && head->num == num){
         ptr2 = head;
         head = head->next;
         free(ptr2);
    }
    if(head == NULL)  /*链表空 */
        return NULL;
     /* 要被删除结点为非表头结点  */
     ptr1 = head;
     ptr2 = head->next; /*从表头的下一个结点搜索所有符合删除要求的结点 */
     while(ptr2 != NULL){
         if(ptr2->num == num){ 	/* ptr2所指结点符合删除要求 */
             ptr1->next = ptr2->next;
             free(ptr2);
         }
         else
             ptr1 = ptr2;       /* ptr1后移一个结点 */
         ptr2 = ptr1->next;    /* ptr2指向ptr1的后一个结点 */
     }
     return head;
}

/*遍历操作*/
void Print_Stu_Doc(struct stud_node * head)
{   struct stud_node * ptr;
    if(head == NULL){
        printf("\nNo Records\n");
        return;
    }
    printf("\nThe Students' Records Are: \n");
    printf("Num\t Name\t Score\n");
    for(ptr = head; ptr != NULL; ptr = ptr->next)
      printf("%d\t%s\t%d \n", ptr->num, ptr->name, ptr->score);

}

#endif // 1

#if 0
/*【例11-8】输入一个字符串和一个字符，如果该字符在字符串中，就从该字符首次出现的位置开始输出字符串中的字符。
例如，输入字符r和字符串program后，输出rogram。要求定义函数match(s, ch)，在字符串s中查找字符ch，如果找到，返回第一次找到的该字符在字符串中的位置（地址）；否则，返回空指针NULL。*/

/* 查找字符串中的字符位置（指针作为函数的返回值）*/
#include <stdio.h>
char *match(char *s, char ch);  	/* 函数声明 */
int main(void )
{
    char ch, str[80], *p = NULL;

    printf("Please Input the string:\n");	 /* 提示输入字符串 */
    scanf("%s", str);
    getchar();         			 /* 跳过输入字符串和输入字符之间的分隔符 */
    ch = getchar();  			     /* 输入一个字符 */
    if((p = match(str, ch)) != NULL)    /* 调用函数match() */
        printf("%s\n", p);
    else
        printf("Not Found\n");
    return 0;
}

char *match(char *s, char ch)  	/* 函数返回值的类型是字符指针 */
{
    while(*s != '\0')
	  if(*s == ch)
        return(s);       /* 若在字符串s中找到字符ch，返回相应的地址 */
	  else
        s++;
    return(NULL);  	/* 在s中没有找到ch，返回空指针 */
}

#endif // 1

#if 0
/*【例11-5】输入一些有关颜色的单词，每行一个，以#作为输入结束标志，再以输入的相反次序输出这些单词。其中单词数小于20，每个单词不超过15个字母（用动态分配内存的方法处理多个字符串的输入）。*/

/* 用动态分配内存方法处理多个字符串的输入 */
#include <stdio.h>
#include<stdlib.h>
#include<string.h>
int main(void)
{
    int i, n = 0;
    char *color[20], str[15];
    printf("Please input some words about color:\n");
    scanf("%s", str);
    while(str[0] != '#') {
        color[n] = (char *)malloc(sizeof(char)*(strlen(str)+1));  /* 动态分配 */
        strcpy(color[n], str);       /* 将输入的字符串赋值给动态内存单元 */
	    n++;
        scanf("%s", str);
    }
    printf("These words are:");
    for(i = n-1; i >= 0; i--){          /* 反序输出 */
        printf("%s  ", color[i]);
        free(color[i]);             /* 释放动态内存单元 */
	}

    return 0;
}

#endif // 1

#if 0
/*【例11-6】解密藏头诗。所谓藏头诗，就是将一首诗每一句的第一个字连起来，所组成的内容就是该诗的真正含义。编写程序，输出一首藏头诗的真实含义。*/

/* 解密藏头诗，指针数组操作 */
#include <stdio.h>
char *change(char s[][20]);
int main(void)
{
   int i;
   char *poem[4] = { "一叶轻舟向东流，", "帆梢轻握杨柳手，", "风纤碧波微起舞，", "顺水任从雅客悠。"};    /* 指针数组初始化 */
   char mean[10];
   for(i = 0; i < 4; i++){  /* 每行取第1个汉字存入mean */
      mean[2 * i] = *(poem[i]);
      mean[2 * i + 1] = *(poem[i] + 1);
   }
   mean[2 * i] = '\0';
   printf("%s\n", mean);    /* 输出结果 */

   return 0;
}
#endif // 1

#if 0
void main( )
{   int i;
    int a[5] = {6, 5, 2, 8, 1};
    int b[5] = {6, 5, 2, 8, 1};


    void fsort(int a[ ], int n);
    void fsort2(int *a, int n);
    fsort(a, 5);
    fsort2(b, 5);
    for(i = 0; i < 5; i++)
        printf("%d ", a[i]);
 }

void fsort(int a[ ], int n)
{     int k, j;
      int temp;
      for(k = 1; k < n; k++)
         for(j = 0; j < n-k; j++)
             if(a[j] > a[j+1]){
                  temp = a[j];
                  a[j] = a[j+1];
                  a[j+1] = temp;
            }
}

void fsort2(int *a, int n)
{     int k, j;
      int temp;
      for(k = 1; k < n; k++)
         for(j = 0; j < n-k; j++)
             if(a[j] > a[j+1]){
                  temp = a[j];
                  a[j] = a[j+1];
                  a[j+1] = temp;
            }
}
#endif // 1

#if 0

/*【例11-4】将5个字符串从小到大排序后输出。*/

#include <stdio.h>
#include <string.h>
int main(void )
{
    int i;
    char *pcolor[ ] = {"red", "blue", "yellow",
 "green", "black"};
    void fsort(char *color[ ], int n);

    fsort(pcolor, 5);   /* 调用函数 */
    for(i = 0; i < 5; i++)
        printf("%s ", pcolor[i]);

    return 0;
}

void fsort(char *color[ ], int n)
{
    int k, j;
    char *temp;
    for(k = 1; k < n; k++)
      for(j = 0; j < n-k; j++)
        if(strcmp(color[j], color[j+1]) > 0){
          temp = color[j];
          color[j] = color[j+1];
          color[j+1] = temp;
        }
}


#endif // 1

#if 0
/*【例11-3】使用二级指针方式改写例11-1。*/

/* 找奥运五环色的位置，用二级指针操作指针数组 */
#include<stdio.h>
#include<string.h>
int main(void)
{
  int i;
  char *color[5] = {"red", "blue", "yellow", "green", "black"};  /* 指针数组初始化 */
  char **pc;  /*定义二级指针变量*/
  char str[20];
  pc = color;    /* 二级指针赋值 */
  printf("Input a color:");
  scanf("%s", str);
  for(i = 0; i < 5; i++)
    if(strcmp(str, *(pc+i)) == 0)  /* 比较颜色是否相同 */
      break;
  if(i < 5)
    printf("position:%d\n", i+1);
  else
    printf("Not Found\n");

  return 0;
}

#endif // 0

/*【例11-1】已知奥运五环的5种颜色的英文单词按一定顺序排列，输入任意一个颜色的英文单词，从已有颜色中查找并输出该颜色的位置值，若没有找到，则输出"Not Found"。*/

/* 查找奥运五环色的位置，用指针数组实现 */
#if 0
#include<stdio.h>
#include<string.h>
int main()
{
   int i;
   char *p="blue";//（类型 *）告诉我们要定义一个指针来放地址了。这
         //个指针名字叫p，p得到地址初值即放在系统常量区的”blue”的地址 。
   char str[20];
   scanf("%s", str);
   int cmp = (strcmp(str, p) == 0);// strcmp是字符串比较函数
   printf("color: str %s, p %s, cmp %d", str, p, cmp);

   char *color[5] = {"red", "blue", "yellow", "green", "black" };  /* 指针数组初始化 */

   for(i = 0; i < 5; i++)
     if(strcmp(str, color[i]) == 0)  /* 比较颜色是否相同 */
       break;
   if(i < 5)
     printf("position:%d\n", i+1);
   else
     printf("Not Found\n");

   return 0;
}
#endif // 0